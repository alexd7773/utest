import * as yup from 'yup';

export const userSchema = yup.object().shape({
	first_name: yup.string().max(256, 'Максимальная длина имени 256 символов').required('Имя необходимо ввести'),
	last_name: yup.string().max(256, 'Максимальная длина фамилии 256 символов').required('Фамилию необходимо ввести'),
	birth_date: yup.string().required('Необходимо выбрать дату рождения'),
	gender: yup.string().required('Необходимо выбрать пол'),
	job: yup.string().max(256, 'Максимальная длина названия профессии 256 символов').required('Необходимо указать место работы'),
	biography: yup.string().max(1024, 'Максимальная длина текста биографии 1024 символов').required('Необходимо ввести данные о себе'),
});
