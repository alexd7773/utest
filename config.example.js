module.exports = {
	serverURL: process.env.NODE_ENV === 'production' ? 'http://127.0.0.1:3000' : 'http://localhost:3000',
	frontServerUrl: 'https://utest.agency',
	backServerUrl: 'https://frontend-candidate.dev.sdh.com.ua',
};
