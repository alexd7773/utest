/**
 * external libs
 */
import React, { useContext } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
/**
 * utils
 */
import { sender } from './../../../utils/sender';
/**
 * context
 */
import { AlertContext } from './../../common/alert.context';
/**
 * data
 */
import { backServerUrl } from './../../../config';
/**
 * interfaces
 */
import { User } from '../../../types/interfaces';
/**
 * styles
 */
import styles from './../../../styles/info.module.scss';

const InvoView: React.FunctionComponent<{
	user: User;
}> = ({ user }) => {
	const router = useRouter();
	const { showAlert } = useContext(AlertContext);

	const delUser: (id: number) => void = async (id) => {
		const delRequest = await sender(`${backServerUrl}/v1/contact/${id}/`, 'DELETE');

		if (delRequest?.res !== 'success') {
			showAlert('Произошла ошибка', 'error');
		} else {
			showAlert('Пользователь удален', 'success');
			router.replace('/');
		}
	};

	return (
		<div className={styles.info__content}>
			<div className={styles.info__data}>
				<div className={styles.info__part}>
					<p className={styles.info__label}>Имя</p>
					<p className={styles.info__value}>{user.first_name}</p>
				</div>

				<div className={styles.info__part}>
					<p className={styles.info__label}>Фамилия</p>
					<p className={styles.info__value}>{user.last_name}</p>
				</div>

				<div className={styles.info__part}>
					<p className={styles.info__label}>Дата рождения</p>
					<p className={styles.info__value}>{user.birth_date}</p>
				</div>

				<div className={styles.info__part}>
					<p className={styles.info__label}>Пол</p>
					<p className={styles.info__value}>{user.gender}</p>
				</div>

				<div className={styles.info__part}>
					<p className={styles.info__label}>Профессия</p>
					<p className={styles.info__value}>{user.job}</p>
				</div>

				<div className={styles.info__part}>
					<p className={styles.info__label}>Биография</p>
					<p className={styles.info__value}>{user.biography}</p>
				</div>

				<div className={`${styles.info__check}  ${user.is_active ? styles.active : ''}`}>
					<p className={styles.info__label}>{user.is_active ? 'Активен' : 'Неактивен'}</p>
				</div>
			</div>

			<div className={styles.info__btns}>
				<Link href={`/user/${user.id}`}>
					<a className={`${styles.buttonlink} ${styles.effect}`}>Редактировать</a>
				</Link>

				<div
					className={`${styles.buttonlink} ${styles.table__link_trash} ${styles.effect}`}
					onClick={() => delUser(user.id)}
				>
					Удалить
				</div>
			</div>
		</div>
	);
};

export default InvoView;
